from model import create_model
import numpy as np
import facenet_keras
import os
import cv2
try:
    import os

    os.add_dll_directory(os.path.join(os.environ['CUDA_PATH'], 'bin'))
except Exception:
    pass
from align import AlignDlib

alignment = AlignDlib('landmarks.dat')


def load_image(path):
    img = cv2.imread(path, 1)
    return img[..., ::-1]


def invert_frame(img):
    return img[..., ::-1]


def align_image(img):
    return alignment.align(96, img, alignment.getLargestFaceBoundingBox(img),
                           landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)


model = facenet_keras.facenet()
model.load_weights('weights/nn4.small2.v1.h5')
images = []
num_images = 0
for i in os.listdir('images/users'):
    num_images += 1
    img = load_image('images/users/' + i)
    img = align_image(img)
    img = (img / 255.).astype(np.float32)
    images.append([img, i[0:len(i)-4]])


for i, m in enumerate(images):
    encoding = model.predict(np.expand_dims(images[i][0], axis=0))[0]
    np.save('images/users/' + images[i][1] + '.npy', encoding)

