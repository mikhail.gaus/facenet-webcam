from model import create_model
import facenet_keras
import cv2
import time
import numpy as np
import sys
import logging as log
import datetime as dt
from time import sleep

try:
    import os

    os.add_dll_directory(os.path.join(os.environ['CUDA_PATH'], 'bin'))
except Exception:
    pass
from align import AlignDlib
import matplotlib.pyplot as plt

model = facenet_keras.facenet()
model.load_weights('weights/nn4.small2.v1.h5')
threshold = 0.65

alignment = AlignDlib('landmarks.dat')


def load_image(path):
    img = cv2.imread(path, 1)
    return img[..., ::-1]


def invert_frame(img):
    return img[..., ::-1]


def align_image(img):
    return alignment.align(96, img, alignment.getLargestFaceBoundingBox(img),
                           landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)


def distance(emb1, emb2):
    return np.sum(np.square(emb1 - emb2))


# Preprocess images and load them
encodings = []
for i in os.listdir('images/users'):
    if i[-4:] == '.npy':
        encoding = np.load('images/users/'+i)
        encodings.append([encoding, i[0:-4]])

video_capture = cv2.VideoCapture(0)
anterior = 0
font = cv2.FONT_HERSHEY_PLAIN
while True:
    if not video_capture.isOpened():
        print('Unable to load camera.')
        sleep(5)
        pass

    # Capture frame-by-frame
    ret, frame = video_capture.read()
    face = alignment.getLargestFaceBoundingBox(frame)
    if face is not None:
        x = face.tl_corner().x
        y = face.tl_corner().y
        w = face.width()
        h = face.height()
        img = invert_frame(frame)

        img = align_image(img)
        img = (img / 255.).astype(np.float32)
        encoding1 = model.predict(np.expand_dims(img, axis=0))[0]
        name = ''

        for en in encodings:
            difference = distance(encoding1, en[0])
            if difference < threshold:
                name = en[1]

        cv2.rectangle(frame, (x, y), (x + w, y + h), (71, 40, 82), 2)
        cv2.putText(frame, name, (x, y + 30), font, 1, (71, 40, 82), 2)
        # Display the resulting frame
    cv2.imshow('Video', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

        # Display the resulting frame
    cv2.imshow('Video', frame)

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
