# FaceNet-Webcam

Project is build on base of : https://github.com/TessFerrandez/research-papers/tree/master/facenet

And the original paper of FaceNet: https://www.cv-foundation.org/openaccess/content_cvpr_2015/html/Schroff_FaceNet_A_Unified_2015_CVPR_paper.html

You will need Python 3 and pip installed.

`git clone https://gitlab.com/mikhail.gaus/facenet-webcam.git`

next do:

`pip install keras`

then:

`pip install opencv-python`

after that:

`pip install face-recognition`

and just one more:

`pip install pandas`

In default mode script will work as face detector.
If you want the facenet to regonize your(or someones else) particular face, put your pictures in images/users directory and run:

`python3 pre-predict_images.py`

then run:

`python3 webcam.py`

Press 'Q' to quit.

This should do the trick.
Have fun.